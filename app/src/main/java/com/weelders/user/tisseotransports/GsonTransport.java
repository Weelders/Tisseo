package com.weelders.user.tisseotransports;


import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.weelders.user.tisseotransports.model.LinesCreator;


/**
 * This project is make by Rodolphe on 29/08/2018
 */
public class GsonTransport extends AsyncTask<String,Void,String>
    {

    private OnTaskCompleted mCallBack;
    private Context mContext;


    public GsonTransport(Context context, OnTaskCompleted callBack){
        mContext = context;
        mCallBack = callBack;
    }


    @Override
    protected void onPostExecute(String s)
        {
        super.onPostExecute(s);
       // System.out.println(s);

        Gson gson = new Gson();
        LinesCreator linesCreator = gson.fromJson(s,LinesCreator.class);
        //System.out.println(linesCreator.getLines().getLine().get(0).getName());

        mCallBack.updateUI(linesCreator);

        }

    @Override
    protected String doInBackground(String... strings)
        {
        return new TisseoHTTPSConnection().getJson();
        }



    }
