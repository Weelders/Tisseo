
package com.weelders.user.tisseotransports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Line {

    @SerializedName("bgXmlColor")
    @Expose
    private String bgXmlColor;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("fgXmlColor")
    @Expose
    private String fgXmlColor;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("network")
    @Expose
    private String network;
    @SerializedName("reservationMandatory")
    @Expose
    private String reservationMandatory;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("transportMode")
    @Expose
    private TransportMode transportMode;

    public String getBgXmlColor() {
        return bgXmlColor;
    }

    public void setBgXmlColor(String bgXmlColor) {
        this.bgXmlColor = bgXmlColor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFgXmlColor() {
        return fgXmlColor;
    }

    public void setFgXmlColor(String fgXmlColor) {
        this.fgXmlColor = fgXmlColor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getReservationMandatory() {
        return reservationMandatory;
    }

    public void setReservationMandatory(String reservationMandatory) {
        this.reservationMandatory = reservationMandatory;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public TransportMode getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(TransportMode transportMode) {
        this.transportMode = transportMode;
    }

}
