package com.weelders.user.tisseotransports;



import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

/**
 * This project is make by Rodolphe on 29/08/2018
 */
public class TisseoHTTPSConnection
    {
    private final static String SERVICE_NAME = "lines";
    private final static String URL_BASE = "https://api.tisseo.fr/v1/";
    private final static String FORMAT = ".json";
    private final static String PARAMETER = "?&key=";
    private final static String API_KEY = "76b5d53a-2130-4e1b-af48-46203bd7650a";

    private static String mURL = URL_BASE+SERVICE_NAME+FORMAT+PARAMETER+API_KEY;

    public String getJson(){
    System.out.println(mURL);
    return getData(mURL);
    }


    public static String getData(String url) {
    HttpsURLConnection con = null;
    InputStream is = null;


    try
        {
        con = (HttpsURLConnection) (new URL(url)).openConnection();
        con.setRequestMethod("GET");
        con.setDoInput(true);
        con.setDoOutput(false);

        con.setRequestProperty("user-key", API_KEY);
        con.connect();

            /* Let's read the response */
        StringBuffer buffer = new StringBuffer();
        is = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        while ((line = br.readLine()) != null)
            buffer.append(line + "\r\n");

        is.close();
        con.disconnect();
        return buffer.toString();
        }
    catch (Throwable t)
        {
        t.printStackTrace();
        }
    finally
        {
        try
            {
            is.close();
            }
        catch (Throwable t)
            {
            }
        try
            {
            con.disconnect();
            }
        catch (Throwable t)
            {
            }
        }
    return "";

    }

    }
