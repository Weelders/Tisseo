
package com.weelders.user.tisseotransports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LinesCreator {

    @SerializedName("expirationDate")
    @Expose
    private String expirationDate;
    @SerializedName("lines")
    @Expose
    private Lines lines;

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Lines getLines() {
        return lines;
    }

    public void setLines(Lines lines) {
        this.lines = lines;
    }

}
