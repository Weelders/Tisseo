package com.weelders.user.tisseotransports;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.weelders.user.tisseotransports.model.LinesCreator;

import java.util.ArrayList;

/**
 * This project is make by Rodolphe on 29/08/2018
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder>
    {
    private LinesCreator linesCreator;



    public RecyclerViewAdapter(LinesCreator linesCreator)
        {
        this.linesCreator = linesCreator;
        }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row,parent,false);
        ViewHolder mViewHolder = new ViewHolder(itemView);
        return mViewHolder;
        }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
        {

        if((linesCreator.getLines().getLine().get(position).getTransportMode()!=null))
            {

            switch (linesCreator.getLines().getLine().get(position).getTransportMode().getName())
                {
                case "métro":
                    holder.miv_icon.setBackgroundResource(R.drawable.metro);
                    holder.miv_icon.setOnLongClickListener(new View.OnLongClickListener()
                        {
                        @Override
                        public boolean onLongClick(View v)
                            {
                            Toast.makeText(holder.mtv_line_name.getContext(), "Métro", Toast.LENGTH_SHORT).show();
                            return false;
                            }
                        });
                    break;

                case "tramway":
                    holder.miv_icon.setBackgroundResource(R.drawable.tram);
                    holder.miv_icon.setOnLongClickListener(new View.OnLongClickListener()
                        {
                        @Override
                        public boolean onLongClick(View v)
                            {
                            Toast.makeText(holder.mtv_line_name.getContext(), "Tramway", Toast.LENGTH_SHORT).show();
                            return false;
                            }
                        });
                    break;

                case "bus":
                    holder.miv_icon.setBackgroundResource(R.drawable.bus);
                    holder.miv_icon.setOnLongClickListener(new View.OnLongClickListener()
                        {
                        @Override
                        public boolean onLongClick(View v)
                            {
                            Toast.makeText(holder.mtv_line_name.getContext(), "Bus", Toast.LENGTH_SHORT).show();
                            return false;
                            }
                        });
                    break;
                default:
                    holder.miv_icon.setBackgroundResource(R.drawable.trdm);
                    holder.miv_icon.setOnLongClickListener(new View.OnLongClickListener()
                        {
                        @Override
                        public boolean onLongClick(View v)
                            {
                            Toast.makeText(holder.mtv_line_name.getContext(), "Transport à la demande", Toast.LENGTH_SHORT).show();
                            return false;
                            }
                        });
                }
            }

        else {
        holder.miv_icon.setBackgroundResource(R.drawable.noi);
        holder.miv_icon.setOnLongClickListener(new View.OnLongClickListener()
            {
            @Override
            public boolean onLongClick(View v)
                {
                Toast.makeText(holder.mtv_line_name.getContext(), "Transport inconnu", Toast.LENGTH_SHORT).show();
                return false;
                }
            });
        }

        holder.mbtn_line_id.setText(linesCreator.getLines().getLine().get(position).getShortName());
        holder.mbtn_line_id.setBackgroundColor(Color.parseColor(linesCreator.getLines().getLine().get(position).getBgXmlColor()));
        holder.mbtn_line_id.setTextColor(Color.parseColor(linesCreator.getLines().getLine().get(position).getFgXmlColor()));
        holder.mtv_line_name.setText(linesCreator.getLines().getLine().get(position).getName());

        }

    @Override
    public int getItemCount()
        {
        return linesCreator.getLines().getLine().size();
        }

    }
