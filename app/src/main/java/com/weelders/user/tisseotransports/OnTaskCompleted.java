package com.weelders.user.tisseotransports;


/**
 * This project is make by Rodolphe on 29/08/2018
 */
public interface OnTaskCompleted<LinesCreator>{
    public void updateUI(LinesCreator linesCreator);
}
