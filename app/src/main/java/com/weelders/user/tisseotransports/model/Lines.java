
package com.weelders.user.tisseotransports.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lines {

    @SerializedName("line")
    @Expose
    private List<Line> line = null;

    public List<Line> getLine() {
        return line;
    }

    public void setLine(List<Line> line) {
        this.line = line;
    }

}
