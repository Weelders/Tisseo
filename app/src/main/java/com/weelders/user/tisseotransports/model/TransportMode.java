
package com.weelders.user.tisseotransports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransportMode {

    @SerializedName("article")
    @Expose
    private String article;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
