package com.weelders.user.tisseotransports;



import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;


import com.weelders.user.tisseotransports.model.LinesCreator;


public class MainActivity extends AppCompatActivity
    {

    private RecyclerView mrecyclerview;
    private RecyclerViewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mrecyclerview = findViewById(R.id.recyclerview);

        GsonTransport gsonTransport = new GsonTransport(this, new OnTaskCompleted<LinesCreator>()
            {
            @Override
            public void updateUI(LinesCreator linesCreator)
                {

                mAdapter = new RecyclerViewAdapter(linesCreator);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                mrecyclerview.setLayoutManager(mLayoutManager);
                mrecyclerview.setItemAnimator(new DefaultItemAnimator());
                mrecyclerview.setAdapter(mAdapter);

                }
            });

        gsonTransport.execute();


        }

    }
