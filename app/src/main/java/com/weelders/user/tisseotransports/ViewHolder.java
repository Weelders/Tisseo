package com.weelders.user.tisseotransports;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This project is make by Rodolphe on 29/08/2018
 */
public class ViewHolder extends RecyclerView.ViewHolder
    {
    public ImageView miv_icon;
    public TextView mtv_line_name;
    public Button mbtn_line_id;



    public ViewHolder(View view)
        {
        super(view);
        miv_icon = view.findViewById(R.id.iv_icon);
        mtv_line_name = view.findViewById(R.id.tv_line_name);
        mbtn_line_id = view.findViewById(R.id.btn_line_id);

        }

    }

